new Vue({
    el: "#vue",
    data: {
            cep:'',
            endereco: {},
            naoLocalizado: false
    },
    attached: function(){
        jQuery(this.$els.cep).mask('00000-000');
    },
    methods: {
        buscar: function(){
            var self = this; //preserva o escopo fora da chamada.
            self.endereco = {};
            self.naoLocalizado = false;
            if (/^[0-9]{5}-[0-9]{3}$/.test(this.cep)) {
                jQuery.getJSON('http://viacep.com.br/ws/'+this.cep+'/json', function(endereco){
                    if (endereco.erro) {
                        // $els é uma variável especial do vue que vai guardar uma referência a todas as diretivas el
                        jQuery(self.$els.logradouro).focus();
                        self.naoLocalizado = true;
                        return;
                    }
                    self.endereco = endereco;
                    jQuery(self.$els.numero).focus();
                });
            }
        }
    }
});
